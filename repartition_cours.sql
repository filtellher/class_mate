DROP DATABASE IF EXISTS projet_cours;
CREATE DATABASE projet_cours;

use PROJET_COURS;
DROP TABLE IF EXISTS `groupe`;
DROP TABLE IF EXISTS `file_attente`;
DROP TABLE IF EXISTS `cours`;
DROP TABLE IF EXISTS `professeur`;
DROP TABLE IF EXISTS `programme`;
DROP TABLE IF EXISTS `user`;




CREATE TABLE `projet_cours`.`user`(
	`COURRIEL` 		 varchar(50) NOT NULL,
	`PRENOM` 		 varchar(50) NOT NULL,
	`NOM` 			 varchar(50) NOT NULL,
	`PASSWORD`		 varchar(16) NOT NULL,
	`ADMINISTRATEUR` bit NULL DEFAULT 0,
	`HEURE_TRAVAIL`  float (2) NOT NULL DEFAULT 0,
	`ACTIF` bit NOT NULL DEFAULT 1,
	-- Pas besoin de desc.
	-- Prof n'existe plus (Juste condition si admin ou non)
	PRIMARY KEY (`COURRIEL`)
)ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `projet_cours`.`cours`(
	`COURS_ID` 			varchar (50) NOT NULL,
	`TITRE` 			varchar (50) NOT NULL,
	`DESCRIPTION` 		varchar (255) NOT NULL,
	`HEURE_THEORIQUE`   float (2) 	NOT NULL,
	`HEURE_LAB`  		float (2) 	NOT NULL,
	`VISIBLE`  			bit 		NULL DEFAULT 0,
	PRIMARY KEY (`COURS_ID`)
	
)ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `projet_cours`.`file_attente`(
	`ID` int NOT NULL AUTO_INCREMENT,
	`COURS_ID` varchar (50) NOT NULL,
	`COURRIEL` varchar (50) NOT NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `FK_ATTENTE_IDCOURS` FOREIGN KEY (`COURS_ID`) REFERENCES `projet_cours`.`cours` (`COURS_ID`),
	CONSTRAINT `FK_ATTENTE_USERID` FOREIGN KEY (`COURRIEL`) REFERENCES `projet_cours`.`user` (`COURRIEL`)
)ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `projet_cours`.`groupe`(
	`ID_GROUPE`	 	varchar (50) NOT NULL,
	`NUM_GROUPE` 	varchar (50) NOT NULL,
	`COURS_ID` 		varchar (50) NOT NULL,
	`NUM_LOCAL` 	varchar (50) NOT NULL,
	`PROFESSEUR` 		varchar (50) NULL,
	PRIMARY KEY (`ID_GROUPE`),
	CONSTRAINT `FK_IDCOURS` FOREIGN KEY (`COURS_ID`) REFERENCES `projet_cours`.`cours` (`COURS_ID`),
	CONSTRAINT `FK_USERID` FOREIGN KEY (`PROFESSEUR`) REFERENCES `projet_cours`.`user` (`COURRIEL`)

)ENGINE=MyISAM DEFAULT CHARSET=latin1;

   

INSERT INTO `user` (`COURRIEL`,`PRENOM`,`NOM`,`PASSWORD`,`ADMINISTRATEUR`,`HEURE_TRAVAIL`)
VALUES ('paulr@crosemont.qc.ca', 'Paul', 'Régis', '123456', 1,0),
	   ('emiliec@crosemont.qc.ca', 'Emilie', 'Charet', '123456', 0,0),
	   ('mariot@crosemont.qc.ca', 'Mario', 'Tremblay',  '123456',0,0),
	   ( 'robertg@crosemont.qc.ca','Robert', 'Gagnon',  '123456',0,0),
	   ('jonathanr@crosemont.qc.ca','Jonathan', 'Robichet',  '123456',0,0),
	   ( 'kevinb@crosemont.qc.ca','Kevin', 'Bauvin',  '123456',0,0);
	   
INSERT INTO `cours`(`COURS_ID`,`TITRE`,`DESCRIPTION`,`HEURE_THEORIQUE`,`HEURE_LAB`,`VISIBLE`)
VALUES ('201-024-RO', 'Statistiques pour informatique', 'Informatique',4,3,1),
	   ('410-034-RO', 'Gestion de projets et qualité des processus daffaires', 'Informatique',3,2,1),
	   ('420-236-RO', 'Concepts de structuration des données informatiques', 'Informatique',2,2,1),
	   ('420-AU5-RO', 'Conception d\'applications hypermédias I', 'Informatique',3,3,1),
	   ('420-AW6-RO', 'Développement de projets informatiques', 'Informatique',4,2,1);
	   

INSERT INTO `groupe`(`ID_GROUPE`,`NUM_GROUPE`,`COURS_ID`,`NUM_LOCAL`,`PROFESSEUR`)
VALUES ('024-01','01','201-024-RO','B414','paulr@crosemont.qc.ca'),
	   ('034-01','02','410-034-RO','B428','emiliec@crosemont.qc.ca'),
	   ('236-01','03','420-236-RO','B432','mariot@crosemont.qc.ca'),
	   ('AU5-01','04','420-AU5-RO','B327','robertg@crosemont.qc.ca'),
	   ('AW6-01','05','420-AW6-RO','B323','jonathanr@crosemont.qc.ca');
	  
INSERT INTO `file_attente`(`COURS_ID`,`COURRIEL`)
VALUES ('201-024-RO','paulr@crosemont.qc.ca'),
	   ('201-024-RO','paulr@crosemont.qc.ca'),
	   ('201-024-RO','paulr@crosemont.qc.ca');
	   