/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author j_c_l
 */
public class Database {

    private static Connection cnx;
    private static String url, user, password;

    public static Connection getConnexion() {
        url = "jdbc:mysql://localhost/projet_cours?serverTimezone=UTC";
        try {
            if (cnx == null || cnx.isClosed()) {
                cnx = DriverManager.getConnection(url, user, password);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cnx;
    }

    public static void setUrl(String url) {
        Database.url = url;
    }

    public static void setUser(String user) {
        Database.user = user;
    }

    public static void setPassword(String password) {
        Database.password = password;
    }

    public static boolean loadDriver(String pilote) {
        try {
            Class.forName(pilote);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static void close() {
        if (cnx != null) {
            try {
                cnx.close();
            } catch (SQLException ex) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
