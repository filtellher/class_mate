/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.entites;

/**
 *
 * @author j_c_l
 */
public class Cours {
//Pas besoin de nom user

    private String num_cours, titre, description;
    private double heures_theoriques, heures_lab;
    private boolean visible;

    public String getNum_cours() {
        return num_cours;
    }

    public void setNum_cours(String num_cours) {
        this.num_cours = num_cours;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getHeures_theoriques() {
        return heures_theoriques;
    }

    public void setHeures_theoriques(double heures_theoriques) {
        this.heures_theoriques = heures_theoriques;
    }

    public double getHeures_lab() {
        return heures_lab;
    }

    public void setHeures_lab(double heures_lab) {
        this.heures_lab = heures_lab;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
