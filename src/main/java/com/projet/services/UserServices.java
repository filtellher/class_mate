package com.projet.services;

import com.projet.daos.JdbcUserDao;
import com.projet.entites.User;


public class UserServices {
    public static boolean inscrire(User user) {
        return new JdbcUserDao().create(user);
    }
    public static User getUser(String courriel) {
        return new JdbcUserDao().findById(courriel);
    }
}
