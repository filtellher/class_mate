/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.services;

import com.projet.daos.JdbcGroupeDao;
import com.projet.entites.Groupe;
import java.util.List;

/**
 *
 * @author j_c_l
 */
public class GroupeServices {

    public static List<Groupe> getGroupeParUser(String user) {
        return new JdbcGroupeDao().findByUser(user);
    }

    public static boolean creerGroupe(Groupe groupe) {
        return new JdbcGroupeDao().create(groupe);
    }

    public static List<Groupe> getGroupesByCoursId(String id) {
        return new JdbcGroupeDao().findGroupesByCoursId(id);
    }

}
