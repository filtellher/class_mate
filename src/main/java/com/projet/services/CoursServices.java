package com.projet.services;

import com.projet.daos.JdbcCoursDao;
import com.projet.entites.Cours;
import java.util.LinkedList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author fx-fe
 */
public class CoursServices {

    public static List<Cours> testCours() {
        List<Cours> listeCours = new LinkedList();
        Cours cours;

        for (int i = 0; i < 20; i++) {
            cours = new Cours();
            cours.setNum_cours("0001");
            cours.setTitre("Titre test");
            //cours.setNom_user("User test");
            if (i <= 5 || i > 10) {
                cours.setDescription("test");
            } else {
                cours.setDescription(" Ceci est une description test pour s'assurer que l'affichage dans les vues se fait correctement Ceci est une description test pour s'assurer que l'affichage dans les vues se fait correctement Ceci est une description test pour s'assurer que l'affichage dans les vues se fait correctement");
            }
            cours.setHeures_theoriques(i * 0.75);
            cours.setHeures_lab(i * 0.25);
            cours.setVisible(true);
            listeCours.add(cours);

        }

        return listeCours;
    }

    public static List<Cours> getAllCours() {
        return new JdbcCoursDao().findAll();
    }

    public static Cours getCoursById(String idCours) {
        return new JdbcCoursDao().findById(idCours);
    }
    
    public static boolean create(Cours cours) {
        return new JdbcCoursDao().create(cours);
    }
}
