/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.services;

import com.projet.daos.JdbcFileAttenteDao;
import com.projet.entites.FileAttente;
import java.util.List;

/**
 *
 * @author j_c_l
 */
public class FileAttenteServices {
    public static FileAttente getById(int id){
        return new JdbcFileAttenteDao().findById(id);
    }
    public static List<FileAttente> getAllFileAttente() {
        return new JdbcFileAttenteDao().findAll();
    }
    
    public static List<FileAttente> getFileAttenteParCourriel(String courriel){
        return new JdbcFileAttenteDao().findByCourriel(courriel);
    }

    public static FileAttente getIdToDelete(int id) {
        return new JdbcFileAttenteDao().deleteById(id);
    }

    
}
