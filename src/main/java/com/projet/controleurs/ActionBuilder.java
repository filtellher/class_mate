/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import javax.servlet.http.HttpServletRequest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author fx-fe
 */
public class ActionBuilder {

    public static Action getAction(HttpServletRequest request) {
        Action action = null;
        String actionAFaire;

        String servletPath = request.getServletPath();
        servletPath = servletPath.substring(1);
        int i = servletPath.indexOf("/");
        if (i == -1) {
            actionAFaire = servletPath;
        } else {
            actionAFaire = servletPath.substring(0, i);
        }
        i = actionAFaire.indexOf(".");
        if (i != -1) {
            actionAFaire = actionAFaire.substring(0, i);
        }
        switch (actionAFaire) {
            case "listeCours":
                action = new ListeCoursAction();
                break;
            case "login":
                action = new LoginAction();
                break;
            case "logout":
                action = new LogoutAction();
                break;
            case "signup":
                action = new SignUpAction();
                break;
            case "profile":
                action = new ProfileAction();
                break;
            case "cours":
                action = new CoursAction();
                break;
            case "ajouterCoursAjax":
                action = new AjouterCoursAjaxAction();
                break;
            case "ajouterCours":
                action = new AjouterCoursAction();
                break;

            default:
                action = new DefaultAction();
        }
        return action;
    }
}
