/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.Cours;
import com.projet.entites.Groupe;
import com.projet.entites.User;
import com.projet.services.CoursServices;
import com.projet.services.GroupeServices;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fx-fe
 */
public class CoursAction extends AbstractAction {

    @Override
    public String execute() {
        HttpSession session = request.getSession();
        String idCours = request.getParameter("id");
        Cours cours = CoursServices.getCoursById(idCours);
        List<Groupe> groupes = GroupeServices.getGroupesByCoursId(cours.getNum_cours());
        System.out.println("Nb de groupes = " + groupes.size() + " Id Cours : " + cours.getNum_cours());
        request.setAttribute("groupes", groupes);
        request.setAttribute("cours", cours);
        User user = (User) session.getAttribute("user");
        request.setAttribute("user", user);
        return "cours";
    }

}
