/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.Cours;
import com.projet.entites.User;
import com.projet.services.CoursServices;
import com.projet.services.UserServices;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fx-fe
 */
public class SignUpAction extends AbstractAction {

    @Override
    public String execute() {
        //Vérifier si l'utilisateur est déjà connecté, l'envoyer à la page d'accueil:
        HttpSession session = request.getSession();
        if (session != null && session.getAttribute("user") != null) {
            List<Cours> liste = CoursServices.testCours();
            request.setAttribute("data", liste);
            return "listeCours";
        }

        String courriel, prenom, nom, mdp, mdp2;
        String httpMethod = request.getMethod();
        boolean formOK = true;
        if ("post".equalsIgnoreCase(httpMethod)) {
            courriel = request.getParameter("courriel");
            prenom = request.getParameter("prenom");
            nom = request.getParameter("nom");
            mdp = request.getParameter("motDePasse");
            mdp2 = request.getParameter("confirmationMotDePasse");
            if ("".equals(prenom.trim())) {
                request.setAttribute("prenom", "Prénom obligatoire");
                formOK = false;
            }
            if ("".equals(nom.trim())) {
                request.setAttribute("nom", "Nom obligatoire");
                formOK = false;
            }
            if ("".equals(courriel.trim())) {
                request.setAttribute("courriel", "Courriel obligatoire");
                formOK = false;
            }
            if ("".equals(mdp.trim())) {
                request.setAttribute("motDePasse", "Mot de passe obligatoire");
                formOK = false;
            }
            if ("".equals(mdp2.trim())) {
                request.setAttribute("confirmationMotDePasse", "Confirmation du mot de passe obligatoire");
                formOK = false;
            }
            if (!mdp.equals(mdp2)) {
                request.setAttribute("motDePasse2", "Les 2 mots de passe doivent être identiques");
                formOK = false;
            }
            if (!formOK) {
                request.setAttribute("message", "Veuillez corriger les erreurs");
                return "inscription";
            }
            User user = new User();
            user.setCourriel(courriel);
            user.setPrenom(prenom);
            user.setNom(nom);
            user.setPassword(mdp);
            user.setAdministrateur(false);
            if (UserServices.inscrire(user)) {
                request.setAttribute("message", "Inscription effectuée avec succès. Vous pouvez maintenant vous connecter");
                return "login";
            } else {

                request.setAttribute("message", "Inscription refusée. L'adresse " + courriel + " est déjà utilisée");
                return "signup";
            }
        }
        return "signup";
    }
}
