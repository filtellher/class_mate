/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.Cours;
import java.io.IOException;
import java.io.PrintWriter;
import com.google.gson.Gson;
import com.projet.services.CoursServices;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fx-fe
 */
public class AjouterCoursAjaxAction extends AbstractAction implements AjaxAction {

    @Override
    public String execute() {
        
        System.out.println("On rentre dans l'exec du AJAX");
        String id, titre, desc, lab, theorie;
        String httpMethod = request.getMethod();
        boolean formOK = true;
//        boolean visible = true;
        Map<String, String> messages = new HashMap();

        id = request.getParameter("id");
        titre = request.getParameter("titre");
        desc = request.getParameter("desc");
        lab = request.getParameter("lab");
        theorie = request.getParameter("theorie");
        System.out.println("Test voir si param vide : " + request.getParameter("titre"));
//        visible = request.getParameter("visible");
//        if ("".equals(id.trim()) || "".equals(titre.trim()) || "".equals(desc.trim()) || "".equals(lab.trim()) || "".equals(theorie.trim())) {
            if ("".equals(titre.trim())) {
            //request.setAttribute("motDePasse", "Mot de passe obligatoire");
            messages.put("message", "Tous les champs sont obligatoires, veuillez corrigez les erreures.");
            formOK = false;
        }
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            System.out.println("On rentre dans le try");
            if (!formOK) {
                messages.put("message", "Tous les champs sont obligatoires, veuillez corrigez les erreures.");
                String jsonRep = "{\"R\":\"OK\"}";
                out.print(jsonRep);
                return "rien";
            }
            Cours cours = new Cours();
            cours.setNum_cours(id);
            cours.setTitre(titre);
            cours.setDescription(desc);
            cours.setHeures_lab(Double.parseDouble(lab));
            cours.setHeures_theoriques(Double.parseDouble(theorie));
            if (CoursServices.create(cours)) {
                messages.put("message", "Inscription effectuée avec succès. Vous pouvez maintenant vous connecter");
                System.out.println("User créé");
                Gson gson = new Gson();
                //String jsonRep = "{\"messages\":" + gson.toJson(messages) + "}";
                String jsonRep = "{\"R\":\"OK\"}";
                out.print(jsonRep);
                return "rien";
            } else {
                messages.put("message", "Une erreure c'est produite dans l'ajout du cours");
                Gson gson = new Gson();
                String jsonRep = "{\"messages\":"+gson.toJson(messages)+"}";
                out.print(jsonRep);
                return "rien";
            }
        } catch (IOException ex) {
            Logger.getLogger(AjouterCoursAjaxAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "rien";

    }
}
