/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.FileAttente;
import com.projet.entites.Groupe;
import com.projet.entites.User;
import com.projet.services.FileAttenteServices;
import com.projet.services.GroupeServices;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author j_c_l
 */
public class ProfileAction extends AbstractAction{

    @Override
    public String execute() {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        request.setAttribute("user", user);
        List<FileAttente> liste_attente = FileAttenteServices.getFileAttenteParCourriel(user.getCourriel());
        request.setAttribute("attente", liste_attente);
        List<Groupe> liste_groupe = GroupeServices.getGroupeParUser(user.getCourriel());
        request.setAttribute("groupe", liste_groupe);
        
        
        //FileAttenteServices effacer=null; 
        //effacer.getUserToDelete(user.getCourriel());
        //request.setAttribute("effacer", effacer);       
        
        return "profileUser";
    }

}
