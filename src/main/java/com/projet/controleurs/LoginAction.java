/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.Cours;
import com.projet.entites.User;
import com.projet.services.CoursServices;
import com.projet.services.UserServices;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fx-fe
 */
public class LoginAction extends AbstractAction {

    @Override
    public String execute() {
        HttpSession session = request.getSession();
        System.out.println("Session : " + session);
        System.out.println("Session user : " + session.getAttribute("user"));
        if (session != null && session.getAttribute("user") != null) {
            List<Cours> liste = CoursServices.testCours();
            request.setAttribute("data", liste);
            return "index";
        }

        String courriel, mdp;
        String httpMethod = request.getMethod();
        boolean formOK = true;
        if ("post".equalsIgnoreCase(httpMethod)) {
            courriel = request.getParameter("courriel");
            mdp = request.getParameter("motDePasse");
            if ("".equals(courriel.trim())) {
                request.setAttribute("courriel", "Courriel obligatoire");
                formOK = false;
            }
            if ("".equals(mdp.trim())) {
                request.setAttribute("motDePasse", "Mot de passe obligatoire");
                formOK = false;
            }
            if (!formOK) {
                request.setAttribute("message", "Veuillez corriger les erreurs");
                return "login";
            }
            User user = UserServices.getUser(courriel);
            System.out.println("MDP : " + user.getPassword());
            if (user == null || !mdp.equals(user.getPassword())) {
                request.setAttribute("message", "Informations de connexion incorrectes");
                return "login";
            }
            session = request.getSession(true);
            user.setPassword("");
            session.setAttribute("user", user);
            List<Cours> liste = CoursServices.testCours();
            request.setAttribute("data", liste);
            return "index";
        }

        return "login";
    }

}
