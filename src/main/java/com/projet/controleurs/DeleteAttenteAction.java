/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controleurs;

import com.projet.entites.FileAttente;
import com.projet.services.FileAttenteServices;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author j_c_l
 */
public class DeleteAttenteAction extends AbstractAction {

    @Override
    public String execute() {
        //User user = (User) session.getAttribute("user");
        HttpSession session = request.getSession();
        FileAttente test = (FileAttente) session.getAttribute("id");
        int id = test.getId();
        FileAttente fa = FileAttenteServices.getIdToDelete(id);
        request.setAttribute("effacer", fa);
        return "effacerAttente";
    }
    
}
