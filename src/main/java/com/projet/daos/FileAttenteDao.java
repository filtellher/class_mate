/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.FileAttente;
import java.util.List;

/**
 *
 * @author j_c_l
 */
public interface FileAttenteDao {
    public boolean create(FileAttente fa);
    public FileAttente findById(int id);
    public List<FileAttente> findAll();
    public List<FileAttente> findByCourriel(String courriel);
    public FileAttente deleteById(int id);
}
