/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Groupe;
import java.util.List;

/**
 *
 * @author j_c_l
 */
public interface GroupeDao {
    public List<Groupe> findGroupesByCoursId(String id);
    public boolean create(Groupe g);
    public List<Groupe> findByUser(String user);

}
