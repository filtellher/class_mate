/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.FileAttente;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author j_c_l
 */
public class JdbcFileAttenteDao implements FileAttenteDao {

    @Override
    public boolean create(FileAttente fa) {
        String requete = "INSERT INTO file_attente( COURS_ID, COURRIEL) "
                       + "VALUES (?,?)";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return false;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, fa.getCours_id());
            stm.setString(2, fa.getCourriel());

            int n = stm.executeUpdate();
            return n>0;            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcFileAttenteDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close(); 
        return false;
    }

    @Override
    public FileAttente findById(int id) {
        FileAttente fa = null;
        String requete = "SELECT * FROM file_attente WHERE ID=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setInt(1, id);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                fa = new FileAttente();
                fa.setId(res.getInt("ID"));
                fa.setCours_id(res.getString("COURS_ID"));
                fa.setCourriel(res.getString("COURRIEL"));

            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcFileAttenteDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }     
        Database.close();
        return fa;
    }

    @Override
    public List<FileAttente> findAll() {
        List<FileAttente> liste = new LinkedList();
        FileAttente fa;
        String requete = "SELECT * FROM file_attente";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return liste;
        }
        try (
            Statement stm = cnx.createStatement();
            ResultSet res = stm.executeQuery(requete);
        ){
            while (res.next()) {
                fa = new FileAttente();
                fa.setId(res.getInt("ID"));
                fa.setCours_id(res.getString("COURS_ID"));
                fa.setCourriel(res.getString("COURRIEL"));
                liste.add(fa);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcFileAttenteDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close();
        return liste;
    }

    @Override
    public List<FileAttente> findByCourriel(String courriel) {
        List<FileAttente> liste = new LinkedList();
        FileAttente fa;
        String requete = "SELECT * FROM file_attente WHERE COURRIEL=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return liste;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, courriel);
            ResultSet res = stm.executeQuery();
            while (res.next()) {
                fa = new FileAttente();
                fa.setId(res.getInt("ID"));
                fa.setCours_id(res.getString("COURS_ID"));
                fa.setCourriel(res.getString("COURRIEL"));
                liste.add(fa);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcFileAttenteDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close();
        return liste;
    }

    @Override
    public FileAttente deleteById(int id) {
        FileAttente fa = null;
        String requete = "DELETE FROM file_attente WHERE ID=?";
        Connection cnx = Database.getConnexion();                
        if (cnx==null) {
            System.out.println("PAS DE CONNEXION !!!");
        }                
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setInt(1, id);
      
        } catch (SQLException ex) {
            Logger.getLogger(JdbcFileAttenteDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }     
        Database.close();
        return fa;
    }

    
}
