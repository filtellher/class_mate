/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Cours;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author j_c_l
 */
public class JdbcCoursDao implements CoursDao{

    public JdbcCoursDao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cours> findAll() {
        List<Cours> liste = new LinkedList();
        Cours cours;
        String requete = "SELECT * FROM cours";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return liste;
        }
        try (
            Statement stm = cnx.createStatement();
            ResultSet res = stm.executeQuery(requete);
        ){
            while (res.next()) {
                cours = new Cours();
                cours.setNum_cours(res.getString("COURS_ID"));
                cours.setTitre(res.getString("TITRE"));
                cours.setDescription(res.getString("DESCRIPTION"));
                cours.setHeures_theoriques(res.getInt("HEURE_THEORIQUE"));
                cours.setHeures_lab(res.getInt("HEURE_LAB"));
                cours.setVisible(res.getBoolean("VISIBLE"));
                liste.add(cours);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCoursDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close();
        return liste;
    }

    @Override
    public Cours findById(String id) {
        Cours cours = null;
        String requete = "SELECT * FROM cours WHERE COURS_ID=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, id);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                cours = new Cours();
                cours.setNum_cours(res.getString("COURS_ID"));
                cours.setTitre(res.getString("TITRE"));
                cours.setDescription(res.getString("DESCRIPTION"));
                cours.setHeures_theoriques(res.getInt("HEURE_THEORIQUE"));
                cours.setHeures_lab(res.getInt("HEURE_LAB"));
                cours.setVisible(res.getBoolean("VISIBLE"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCoursDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }     
        Database.close();
        return cours;
    }

    @Override
    public Cours findByNom(String titre) {
        Cours cours = null;
        String requete = "SELECT * FROM cours WHERE TITRE=?";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return null;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, titre);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                cours = new Cours();
                cours.setNum_cours(res.getString("COURS_ID"));
                cours.setTitre(res.getString("TITRE"));
                cours.setDescription(res.getString("DESCRIPTION"));
                cours.setHeures_theoriques(res.getInt("HEURE_THEORIQUE"));
                cours.setHeures_lab(res.getInt("HEURE_LAB"));
                cours.setVisible(res.getBoolean("VISIBLE"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCoursDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return cours;
    }

    @Override
    public boolean create(Cours cours) {
        String requete = "INSERT INTO programme(COURS_ID, TITRE, DESCRIPTION, HEURE_THEORIQUE, HEURE_LAB) "
                       + "VALUES (?,?,?,?,?)";
        Connection cnx = Database.getConnexion();
        if (cnx==null) {
            return false;
        }
        try (
            PreparedStatement stm = cnx.prepareStatement(requete);
        ){
            stm.setString(1, cours.getNum_cours());
            stm.setString(2, cours.getTitre());
            stm.setString(3, cours.getDescription());
            stm.setDouble(4, cours.getHeures_theoriques());
            stm.setDouble(5, cours.getHeures_lab());
            

            int n = stm.executeUpdate();
            return n>0;            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCoursDao.class.getName()).log(Level.SEVERE, null, ex);
        }     
        Database.close(); 
        return false;
    }
    
}
