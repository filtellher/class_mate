/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.Groupe;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author j_c_l
 */
public class JdbcGroupeDao implements GroupeDao {

    @Override
    public List<Groupe> findGroupesByCoursId(String id) {
        List<Groupe> liste = new LinkedList();
        Groupe groupe;
        String requete = "SELECT * FROM groupe WHERE COURS_ID=?";
        Connection cnx = Database.getConnexion();
        if (cnx == null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
                PreparedStatement stm = cnx.prepareStatement(requete);) {
            stm.setString(1, id);
            ResultSet res = stm.executeQuery();
            while (res.next()) {
                groupe = new Groupe();
                groupe.setId_groupe(res.getString("ID_GROUPE"));
                groupe.setNum_groupe(res.getString("NUM_GROUPE"));
                groupe.setCours_id(res.getString("COURS_ID"));
                groupe.setNum_local(res.getString("NUM_LOCAL"));
                groupe.setUser_id(res.getString("PROFESSEUR"));
                liste.add(groupe);
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcGroupeDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        Database.close();
        return liste;
    }
    
     @Override
    public List<Groupe> findByUser(String user) {
        List<Groupe> liste = new LinkedList();
        Groupe groupe;
        String requete = "SELECT * FROM groupe WHERE PROFESSEUR=?";
        Connection cnx = Database.getConnexion();
        if (cnx == null) {
            return liste;
        }
        try (
                PreparedStatement stm = cnx.prepareStatement(requete);) {
            stm.setString(1, user);
            ResultSet res = stm.executeQuery();
            while (res.next()) {
                groupe = new Groupe();
                groupe.setId_groupe(res.getString("ID_GROUPE"));
                groupe.setNum_groupe(res.getString("NUM_GROUPE"));
                groupe.setCours_id(res.getString("COURS_ID"));
                groupe.setNum_local(res.getString("NUM_LOCAL"));
                groupe.setUser_id(res.getString("PROFESSEUR"));
                liste.add(groupe);
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcGroupeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return liste;
    }

    @Override
    public boolean create(Groupe groupe) {
        String requete = "INSERT INTO groupe(ID_GROUPE, NUM_GROUPE, COURS_ID, NUM_LOCAL, PROFESSEUR) "
                + "VALUES (?,?,?,?,?)";
        Connection cnx = Database.getConnexion();
        if (cnx == null) {
            return false;
        }
        try (
                PreparedStatement stm = cnx.prepareStatement(requete);) {
            stm.setString(1, groupe.getId_groupe());
            stm.setString(2, groupe.getNum_groupe());
            stm.setString(3, groupe.getCours_id());
            stm.setString(4, groupe.getNum_local());
            stm.setString(5, groupe.getUser_id());

            int n = stm.executeUpdate();
            return n > 0;
        } catch (SQLException ex) {
            Logger.getLogger(JdbcGroupeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

}
