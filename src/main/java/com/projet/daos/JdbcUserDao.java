/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.daos;

import com.projet.entites.User;
import com.projet.jdbc.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fx-fe
 */
public class JdbcUserDao implements UserDao {

    @Override
    public User findById(String courriel) {

        User user = null;
        String requete = "SELECT * FROM user WHERE COURRIEL=?";
        Connection cnx = Database.getConnexion();
        if (cnx == null) {
            System.out.println("PAS DE CONNEXION !!!");
            return null;
        }
        try (
                PreparedStatement stm = cnx.prepareStatement(requete);) {
            stm.setString(1, courriel);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                user = new User();
                user.setCourriel(res.getString("COURRIEL"));
                user.setPrenom(res.getString("PRENOM"));
                user.setNom(res.getString("NOM"));
                user.setPassword(res.getString("PASSWORD"));
                user.setAdministrateur(res.getBoolean("ADMINISTRATEUR"));
                user.setHeure_travail(res.getDouble("HEURE_TRAVAIL"));
             
            }            
        } catch (SQLException ex) {
            Logger.getLogger(JdbcUserDao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        Database.close();
        return user;
    }

    @Override
    public boolean create(User user) {
       // String requete = "INSERT INTO user(COURRIEL, MOT_DE_PASSE, PRENOM, NOM, ADMINISTRATEUR) "
         //       + "VALUES (?,?,?,?,?)";
        String requete = "INSERT INTO user(COURRIEL, PASSWORD, PRENOM, NOM) "
                + "VALUES (?,?,?,?)";
        Connection cnx = Database.getConnexion();
        if (cnx == null) {
            return false;
        }
        try (
                PreparedStatement stm = cnx.prepareStatement(requete);) {
            stm.setString(1, user.getCourriel());
            stm.setString(2, user.getPassword());
            stm.setString(3, user.getPrenom());
            stm.setString(4, user.getNom());
            int n = stm.executeUpdate();
            return n > 0;
        } catch (SQLException ex) {
            Logger.getLogger(JdbcUserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }
}
