/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.listeners;

import com.projet.jdbc.Database;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author j_c_l
 */
public class EcouteurApplication implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext application = event.getServletContext();
        System.out.println("Démarrage de l'application : " + application.getServletContextName());
        String driver = application.getInitParameter("pilote");
        String url = application.getInitParameter("bdurl");
        String user = application.getInitParameter("user");
        String password = application.getInitParameter("password");

        Database.loadDriver(driver);
        Database.setUrl(url);
        Database.setUser(user);
        Database.setPassword(password);
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("Arrêt de l'application : "+ event.getServletContext().getServletContextName());
        Database.close();
    }
    
    
}