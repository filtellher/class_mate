<%-- 
    Document   : login
    Created on : 2 avr. 2021, 17 h 00 min 50 s
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <title>CLASS MATE</title>
    </head>
    <body>
        <jsp:include page='navbar.jsp'></jsp:include>

            <div class="login-content">
                <div class="login-container container">
                    <div class="sign-up">
                        <a href="login" class="primary-btn login-btn login-btn-input">
                            Se connecter
                        </a>
                        <h4>Vous êtes déja enregistré.e sur le site?</h4>
                    </div>
                    <form class="login" method="post" action='signup' id='signUpForm'>
                        <h1 style="color: #26547c">Inscription</h1>
                        <span class='erreur'>${requestScope.message}</span>
                    <span class="erreur">${requestScope.prenom}</span>
                    <input
                        type="text"
                        class="prenom login-input"
                        name="prenom"
                        value="${param.prenom}"
                        placeholder="Prénom"
                        />
                    <span class="erreur">${requestScope.nom}</span>
                    <input
                        type="text"
                        class="nom login-input"
                        name="nom"
                        value="${param.nom}"
                        placeholder="Nom"
                        />
                    <span class="erreur">${requestScope.courriel}</span>
                    <input
                        type="email"
                        class="email login-input"
                        name="courriel"
                        value="${param.courriel}"
                        placeholder="Courriel"
                        />
                    <span class="erreur">${requestScope.motDePasse}</span>
                    <input
                        type="password"
                        class="password login-input"
                        name="motDePasse"
                        placeholder="Mot de passe"
                        />
                    <span class="erreur">${requestScope.confirmationMotDePasse}</span>
                    <input
                        type="password"
                        class="password login-input"
                        name="confirmationMotDePasse"
                        placeholder="Confirmation du mot de passe"
                        />
                    <input
                        type="submit"
                        class="primary-btn sign-up-btn signup-btn-input"
                        value="S'inscrire"
                        />

                    <!--</div>-->
                </form>
            </div>
        </div>
        <jsp:include page='footer.jsp'></jsp:include>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
        </script>
    </body>
</html>
