<%-- 
    Document   : profileUser
    Created on : 21 mai 2021, 23 h 35 min 00 s
    Author     : j_c_l
--%>
<%@page import="com.projet.entites.Groupe"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.projet.services.GroupeServices"%>
<%@page import="com.projet.services.FileAttenteServices"%>
<%@page import="java.util.List"%>
<%@page import="com.projet.entites.FileAttente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page='navbar.jsp'></jsp:include>
        <div class="content">
            <div class="container">
            <c:if test="${not empty sessionScope.user}" >
                <h1>Profile de : ${sessionScope.user.prenom} ${sessionScope.user.nom}</h1>
            </c:if>
            </div>
            
            <div class="container">
                <div class="row"> 
                <c:if test="${empty requestScope.attente}">
                    <h3 class="two">File d'attente Vide</h3>
                </c:if>
                <c:if test="${requestScope.attente.size()>0}">
                    <h1 class="two">Liste de votre file d'attente</h1>
                    <table class="table table-striped table-bordered">
                        <c:forEach items="${requestScope.attente}" var="profileUser" >
                            <tr>
                                <td>${profileUser.id}</td><td>${profileUser.cours_id}</td>
                                <td>${profileUser.courriel}</td><td><a href="#" var="effacer" onclick="${requestScope.effacer}" value="${requestScope.effacer}" >Effacer</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>

        </div>
        <div class="container">
            <div class="row"> 
                <c:if test="${empty requestScope.groupe}">
                    <h3 class="two">Aucun Groupe</h3>
                </c:if>
                <c:if test="${requestScope.groupe.size()>0}">
                    <h1 class="two">Liste de Groupes enregistrés</h1>
                    <table class="table table-striped table-bordered">
                        <c:forEach items="${requestScope.groupe}" var="groupe" >
                            <tr>
                                <td>${groupe.id_groupe}</td><td>${groupe.num_groupe}</td>
                                <td>${groupe.cours_id}</td><td>${groupe.num_local}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
        </div>
        </div>
        <jsp:include page='footer.jsp'></jsp:include>
       
    </body>
</html>
