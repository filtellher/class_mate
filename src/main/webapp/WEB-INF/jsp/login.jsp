<%-- 
    Document   : login
    Created on : 2 avr. 2021, 17 h 00 min 50 s
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <title>CLASS MATE</title>
    </head>
    <body>
        <jsp:include page='navbar.jsp'></jsp:include>

            <!-- Exemple login -->
            <div class="login-content">
                <div class="login-container container">
                    <form class="login" method="post" action='login' id='loginForm'>
                        <!--<div class="login">-->
                        <h1 style="color: #26547c">Connexion</h1>
                        <span class='erreur'>${requestScope.message}</span>
                    <span class="erreur">${requestScope.courriel}</span>
                    <input
                        type="email"
                        class="email login-input"
                        name="courriel"
                        value="${param.courriel}"
                        placeholder="Courriel"
                        />
                    <span class="erreur">${requestScope.motDePasse}</span>
                    <input
                        type="password"
                        class="password login-input"
                        name="motDePasse"
                        placeholder="Mot de passe"
                        />
                    <input
                        type="submit"
                        class="primary-btn login-btn login-btn-input"
                        value="Se connecter"
                        />

                    <!--</div>-->
                </form>
                <div class="sign-up">
                    <a href="signup" class="primary-btn sign-up-btn">
                        S'inscrire
                    </a>
                    <h4>Vous n'êtes pas enregistré.e sur le site?</h4>
                </div>


            </div>
        </div>
        <jsp:include page='footer.jsp'></jsp:include>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
        </script>
    </body>
</html>
