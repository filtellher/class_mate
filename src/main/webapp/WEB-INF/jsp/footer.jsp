<%-- 
    Document   : footer
    Created on : 2021-02-26, 12:34:31
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<footer class="footer container">
    <h3>
        <p>
            <span class="two">Auteurs</span><span class="one"> :</span> &nbsp
            Félix Bonneville-Tellier
            <span class="two">&</span>            
            Jean-Claude Gamarra
        </p>
    </h3>
</footer>

