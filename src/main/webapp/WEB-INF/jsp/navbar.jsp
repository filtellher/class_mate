<%-- 
    Document   : navbar
    Created on : 2021-02-26, 12:31:26
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<nav class="navbar container">
    <div class="nav-title">
        <!-- <div class="nav-icon">
          <i class="fas fa-cat"></i>
        </div> -->
        <h1>
            <a href="index">
                <span class="one">C</span>
                <span class="two">L</span>
                <span class="one">A</span>
                <span class="two">S</span>
                <span class="one">S</span>
                &nbsp
                <span class="two">M</span>
                <span class="one">A</span>
                <span class="two">T</span>
                <span class="one">E</span>
            </a>
        </h1>
    </div>
    <div class="menu">
        <c:if test="${not empty sessionScope.user}">
            <a href="listeCours" class="primary-btn liste-cours-btn">Liste de cours</a>        
            <a href="profile" class="primary-btn login-btn">Profil</a>
            <a href="logout" class="primary-btn sign-up-btn">Se déconnecter</a>        
        </c:if>
        <c:if test="${empty sessionScope.user}">
            <a href="signup" class="primary-btn sign-up-btn">S'inscrire</a>
            <a href="login" class="primary-btn login-btn">Se connecter</a>        
        </c:if>

    </div>
</nav>

