<%-- 
    Document   : cours.jsp
    Created on : 22 mai 2021, 21 h 33 min 43 s
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CLASS MATE</title>
    </head>
    <body>
        <jsp:include page='navbar.jsp'></jsp:include>
        <c:set var="cours" value="${requestScope.cours}"/>
        <c:set var="user" value="${requestScope.user}"/>
        <c:set var="groupes" value="${requestScope.groupes}"/>
        <div class="details-cours-container content">
            <div class="details-cours-header container">
                <div class="details-cours-titre">
                    <h1 class="titre-cours">${cours.titre}</h1>
                    <h2 class="num-cours">${cours.num_cours}</h2>
                    <h3 class="titre-cours">Nombre de groupes disponibles : </h4>
                    <h4 class="heures-cours">${groupes.size()}</h4>
                </div>
                <div class="details-cours-heures">
                    <h3 class="heures-titre">Heures en laboratoire</h3>
                    <h4 class="heures-cours">${cours.heures_lab}H</h4>
                    <h3 class="heures-titre">Heures de théorie</h3>
                    <h4 class="heures-cours">${cours.heures_theoriques}H</h4>
                </div>
            </div>
            <div class="details-cours-infos container">
                <div class="infos-cours">
                    <h2 class="titre-cours">Description du cours</h2>
                    <h4>${cours.description}</h4>
                </div>
                <div class="details-cours-gestion">
                    <c:if test="${user.administrateur}">
                        <c:choose>
                            <c:when test="${groupes.size()>1}">
                                <a href="#" class="primary-btn sign-up-btn btn-cours">Supprimer un groupe</a>
                            </c:when>
                            <c:otherwise>                            
                                <a href="#" class="primary-btn sign-up-btn btn-cours">Supprimer le cours</a>
                            </c:otherwise>
                        </c:choose>
                                <a href="#"></a>
                    </c:if>
                    <a href="#" class="primary-btn accept-btn btn-cours">Choisir ce cours</a>
                </div>


            </div>

        </div>
        <jsp:include page='footer.jsp'></jsp:include>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
        </script>
    </body>
