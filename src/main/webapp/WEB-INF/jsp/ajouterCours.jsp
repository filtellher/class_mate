<%-- 
    Document   : AjouterCours
    Created on : 23 mai 2021, 15 h 45 min 15 s
    Author     : fx-fe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/f4c3172358.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Dosis&display=swap"
            rel="stylesheet"
            />
        <link rel="stylesheet" href="static/style/index.css" />
        <script src="static/javascript/my_ajax.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CLASS MATE</title>
    </head>
    <body>
        <jsp:include page='navbar.jsp'></jsp:include>

            <div class="content">
                <h1 class='titre-cours'>Ajout d'un cours</h1>
                <p id="message" class='erreur'>${requestScope.message}</p>  
            <form class="form login container" method="post" id="addCoursForm" action="ajouterCoursAjaxAction">
                <div class="add-cours-form">
                    <div class="cours-form-container">

                        <label><h4>No. de cours</h4></label>
                        <input type="text" class="form-control login-input" name="id" id="id" value="${param.id}" placeholder="No. de cours"/>

                        <label><h4>Titre du cours</h4></label>
                        <input type="text" class="form-control login-input" name="titre" id="titre" value="${param.titre}" placeholder="Titre du cours" />



<!--<input type="text" class="form-control login-input" name="desc" vlalue="${param.desc}" placeholder="Description du cours" />-->
                        <label><h4>Heures totales de laboratoire</h4></label>
                        <input type="text" class="form-control login-input" name="lab" id="lab" vlalue="${param.lab}" placeholder="Heures totales de laboratoire" />
                    </div>
                    <div class="cours-form-container">


                        <label><h4>Heures totales de théorie</h4></label>
                        <input type="text" class="form-control login-input" name="theorie" id="theorie" vlalue="${param.theorie}" placeholder="Heures totales de théorie" />

                        <label><h4>Description du cours</h4></label>
                        <textarea class='login-input' id="desc"  name="desc" rows="4" cols="50" value="${param.desc}" placeholder="Description du cours" ></textarea>
                        <!--                        <label><h4>Rendre le cours visible</h4></label>
                                                <label class="switch">
                                                    <input type="checkbox" class="" name="visible" vlalue="${param.visible}" placeholder="Visible">
                                                    <span class="slider round"></span>
                                                </label>-->
                    </div>
                </div>
                <input type='button' id="bOK" class='form-control primary-btn accept-btn' value="Ajouter le cours" />
            </form>
        </div>

        <jsp:include page='footer.jsp'></jsp:include>
        <<script>
            var btnOK = document.getElementById("bOK");
            btnOK.onclick = function () {
                var url = "ajouterCoursAjax";
                var rForm = document.getElementById('addCoursForm');
                var donnees = new FormData(rForm);
//                var id = document.getElementById("id").value;
//                var titre = document.getElementById("titre").value;
//                var lab = document.getElementById("lab").value;
//                var theorie = document.getElementById("theorie").value;
//                var desc = document.getElementById("desc").value;
                for (var value of donnees.values()) {
                    console.log(value);
                }
                var champs = document.getElementsByClassName('erreur');
                for (champ of champs) {
                    champ.innerHTML = '';
                }
                ajax(url, function (evt) {
                    var xhr = evt.target;
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        console.log(xhr);

                        var reponseJSON = xhr.responseText;
                        console.log(reponseJSON);
                        var rep = JSON.parse(reponseJSON);


                        if (rep.messages) {
//                            for (msg in rep.messages) {
//                                document.getElementById(msg).innerHTML = rep.messages[msg];
//                            }
                        } else if (rep.R == 'OK') {
                            document.getElementById('message').innerHTML = 'Ajout du cours réussi. Redirection vers la page des cours';
//                            setTimeout("window.location = 'show?v=login'", 3000);
                        }
                    }
//                });
                }, 'POST', donnees);
            }

        </script>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 1000,
            });
        </script>
    </body>
</html>
